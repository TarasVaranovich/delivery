package delivery.data.jpa;


import delivery.data.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;

import delivery.data.model.Goods;


import java.util.List;
import java.util.Set;

@Repository
public interface GoodsRepository extends CrudRepository<Goods, Long> {

    Page<Goods> findAll(Pageable pageable);
    long deleteById(Integer id);
    Set<Goods> findAllByIdIn(Set<Integer> ids);

}

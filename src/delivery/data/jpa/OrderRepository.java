package delivery.data.jpa;

import delivery.data.model.OrderState;
import delivery.data.model.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import delivery.data.model.Order;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface OrderRepository extends CrudRepository<Order, Long> {

   Order findById(Integer id);
   Order findTopByOrderStateOrderByOrderDateAsc(OrderState orderState);
   List<Order> findAllByVehiclesAndOrderState(Vehicle vehicle, OrderState orderState);

   @Query(value = "select * from order_table join PERSON_ORDER on PERSON_ORDER.order_id = order_table.order_id join person on person.person_id = PERSON_ORDER.person_id where person.email = :email limit :limitbegin, :limitsize", nativeQuery = true)
   List<Order> getPersonOrders(@Param("email") String personEmail,
                               @Param("limitbegin") Integer limitBegin,
                               @Param("limitsize") Integer limitSize);

   Page<Order> findAll(Pageable pageable);
   Page<Order> findAllByVehicles(Vehicle vehicle, Pageable pageable);
}

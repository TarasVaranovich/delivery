package delivery.data.jpa;

import delivery.data.model.Order;
import org.springframework.data.repository.CrudRepository;

import delivery.data.model.Person;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

public interface PersonRepository extends CrudRepository<Person, Long> {

	Page<Person> findAll(Pageable pageable);
	Person findBypersonEmail(String personEmail);
}

package delivery.data.jpa;

import delivery.data.model.Order;
import delivery.data.model.OrderState;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import delivery.data.model.Vehicle;

import java.util.List;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

    Vehicle findById(Integer id);
}

package delivery.data.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import delivery.data.model.Warehouse;

@Repository
public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {

    Warehouse findById(Integer id);

}

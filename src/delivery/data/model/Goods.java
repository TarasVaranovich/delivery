package delivery.data.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.FetchType;

@Entity
@Table(name="goods")
public class Goods {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="goods_id", nullable=false)
    private Integer id;
	@Column(name="name", nullable=false)
	private String goodsName;
	@Column(name="cargo", nullable=false)
	private Double goodsCargo;
	@Column(name="volume", nullable=false)
	private Double goodsVolume;
	@Column(name="expiration_date", nullable=true)
	private Date goodsExpirationDate;
	
	public Goods() {
		
		this.goodsName = "default";
		this.goodsCargo = 0.0;
		this.goodsVolume = 0.0;
		this.goodsExpirationDate = null;
	}
	
	public Goods(String goodsName, Double goodsCargo, Double goodsVolume, Date goodsExpirationDate) {
		
		this.goodsName = goodsName;
		this.goodsCargo = goodsCargo;
		this.goodsVolume = goodsVolume;
		this.goodsExpirationDate = goodsExpirationDate;
		
	}

	public Integer getId() {

		return this.id;

	}
	
	public String getName() {
		
		return this.goodsName;
		
	}
	
	public Double getCargo() {
		
		return this.goodsCargo;
		
	}
	
	public Double getVolume() {
		
		return this.goodsVolume;
		
	}
	
	public Date getExpirationDate() {
		
		return this.goodsExpirationDate;
		
	}
	
	/*private Warehouse warehouse;
	public Warehouse getWarehouse() {
		
		return this.warehouse;
		
	}
	
	public void setWarehouse(Warehouse warehouse) {
		
		this.warehouse = warehouse;
		
	}*/
}

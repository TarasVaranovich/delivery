package delivery.data.model;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
abstract class GoodsContainer implements Serializable{
	
	@Id
    @GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="container_id", nullable=false)
    private Integer id;
	@Column(name="location_X", nullable=false)
	protected Double locationPointX;
	@Column(name="location_Y", nullable=false)
	protected Double locationPointY;
	@Column(name="creation_date", nullable=true)
	private Date creationDate;/*BY OCNSTRUCTOR ONLY*/
	@Column(name="max_cargo", nullable=false)
	private Double maxCargo;/*BY OCNSTRUCTOR ONLY*/	
	@Column(name="max_volume", nullable=false)
	private Double maxVolume;/*BY OCNSTRUCTOR ONLY*/
	@Column(name="total_cargo", nullable=false)
	private Double totalCargo;
	@Column(name="volume_rest", nullable=false)
	private Double totalVolume;
	
	GoodsContainer(Double locationPointX,
				   Double locationPointY,
				   Date creationDate, 
				   Double maxCargo, 
				   Double maxVolume) {
		
		this.locationPointX = locationPointX;
		this.locationPointY = locationPointY;
		this.creationDate = creationDate;
		this.maxCargo = maxCargo;
		this.maxVolume = maxVolume;
		this.totalCargo = 0.0;
		this.totalVolume = maxVolume;
		
	}
	public Integer getId() {

		return  this.id;

	}
	public Double getLocationPointX() {
		
		return this.locationPointX;
		
	}
	
	public Double getLocationPointY() {
		
		return this.locationPointY;
		
	}
	
	public Date getCreationDate() {
		
		return this.creationDate;
		
	}
	public Double getMaxCargo() {
		
		return this.maxCargo;
		
	}
	public Double getMaxVolume() {
		
		return this.maxVolume;
		
	}
	public Double getTotalCargo() {
		
		return this.totalCargo;
		
	}
	public Double getTotalVolume() {
		
		return this.totalVolume;
		
	}
	
	protected abstract void recalculateTotalCargo();
	
	protected abstract void recalculateTotalVolume();
}

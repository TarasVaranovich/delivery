package delivery.data.model;

import delivery.data.services.DateConverter;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="order_table")
public class Order implements Comparable<Order>{
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_id", nullable=false)
	Integer id;
	@Column(name="order_location_X", nullable=false)
	private Double locationPointX;
	@Column(name="order_location_Y", nullable=false)
	private Double locationPointY;
	@Column(name="order_date", nullable=true)
	private Date orderDate;
	@Column(name="order_state", nullable=false)
	private OrderState orderState;/*ENUM*/
	
	public Order() {
		DateConverter dc = new DateConverter();
		Date orderDate = dc.getCurrentDate();
		this.locationPointX = 0.0;
		this.locationPointY = 0.0;
		this.orderDate = orderDate;
		this.orderState = OrderState.WAIT;
		
	}
	
	public Order(Double locationPointX, 
				 Double locationPointY, 
				 Date orderDate, 
				 OrderState orderState){
		
		this.locationPointX = locationPointX;
		this.locationPointY = locationPointY;
		this.orderDate = orderDate;
		this.orderState = orderState;
		
	}
	
	public void setLocation(Double locationPointX, Double locationPointY) {
		
		this.locationPointX = locationPointX;
		this.locationPointY = locationPointY;
		
	}
	
	public void setState(OrderState orderState) {
		
		this.orderState = orderState;
		
	}

	public  Integer getId() {

		return  this.id;

	}
	
	public Double getLocationPointX() {
		
		return this.locationPointX;
		
	}
	
	public Double getLocationPointY() {
		
		return this.locationPointY;
		
	}
	
	public Date getDate() {
		
		return this.orderDate;
		
	}
	
	public OrderState getState() {
		
		return this.orderState;
		
	}
	@Override
	public int compareTo(Order order) {
		if(this.getDate().compareTo(order.getDate()) == 0) {

			return 0;

		} else if(this.getDate().compareTo(order.getDate()) == 1) {

			return 1;

		} else  {

			return -1;

		}
	}
	
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinTable(name = "ORDER_GOODS", 
	   joinColumns = @JoinColumn(name = "order_id"), 
	   inverseJoinColumns = @JoinColumn(name = "goods_id"))
	private Set<Goods> goods;
	public Set<Goods> getGoods() {
		
		return this.goods;
		
	}
	
	public void setGoods(Set<Goods> goods) {
		
		this.goods = goods;
		
	}
	//vehicle constraint
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "orders") 
	private Set<Vehicle> vehicles;
	public Set<Vehicle> getVehicles() {
		return this.vehicles;
	}
	
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
}

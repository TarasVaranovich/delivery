package delivery.data.model;

public enum OrderState {
	
	WAIT, PERFORMING, COMPLETED;
}

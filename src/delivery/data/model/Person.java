package delivery.data.model;

import java.util.Set;
import java.util.SortedSet;

import javax.persistence.*;

@Entity
@Table(name="person")
public class Person {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="person_id", nullable=false)
    private Integer id;
	@Column(name="name", nullable=false)
	private String personName;
	@Column(name="surname", nullable=false)
	private String personSurname;
	@Column(name="phone_number", nullable=false)
	private String personPhoneNumber;
	@Column(name="email", nullable=false)
	private String personEmail;
	
	public Person() {

		this.personName = "default";
		this.personSurname = "default";
		this.personPhoneNumber = "0000000";
		this.personEmail = "deafulet@def";
	}
	
	public Person(String personName, 
				  String personSurname, 
				  String personPhoneNumber, 
				  String personEmail) {
		
		this.personName = personName;
		this.personSurname = personSurname;
		this.personPhoneNumber = personPhoneNumber;
		this.personEmail = personEmail;
		
	}
	
	public String getName() {
		
		return this.personName;
		
	}
	
	public String getSurname() {
		
		return this.personSurname;
		
	}
	
	public String getPhoneNumber() {
		
		return this.personPhoneNumber;
		
	}
	
	public String getEmail() {
		
		return this.personEmail;
		
	}
	
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinTable(name = "PERSON_ORDER", 
	   joinColumns = @JoinColumn(name = "person_id"), 
	   inverseJoinColumns = @JoinColumn(name = "order_id"))
	@OrderBy("orderDate ASC")
	private SortedSet<Order> orders;
	public SortedSet<Order> getOrders() {
		
		return this.orders;
		
	}
	
	public void setOrders(SortedSet<Order> orders) {
		
		this.orders = orders;
		
	}
}

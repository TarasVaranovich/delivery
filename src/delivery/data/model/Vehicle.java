package delivery.data.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="vehicle")
public class Vehicle extends GoodsContainer {

	@Column(name="model", nullable=false)
	private String vehicleModel; /*BY COCNSTRUCTOR ONLY*/
	@Column(name="speed", nullable=false)
	private Double vehicleSpeed;
	@Column(name="state", nullable=false)
	private VehicleState vehicleState; /*ENUM*/
	@Column(name="register_date", nullable=true)
	private Date vehicleRegistrationDate;/*BY OCNSTRUCTOR ONLY*/
	private GoodsContainer goodsContainer;

	public Vehicle() {	
		super(0.0, 
			  0.0, 
			  null, 
			  0.0, 
			  0.0);
		
		this.vehicleModel = "default";
		this.vehicleSpeed = 0.0;
		this.vehicleState = VehicleState.FREE;	
		this.vehicleRegistrationDate = null;
	}
	
	public Vehicle(String vehicleModel, 
			Date vehicleRegistrationDate, 
			Date vehicleProductionDate, 
			Double vehicleMaxCargo, 
			Double vehicleMaxVolume, 
			Double vehicleCurrentPointX,
			Double vehicleCurrentPointY) {
		
		super(vehicleCurrentPointX,
			  vehicleCurrentPointY,
			  vehicleProductionDate, 
			  vehicleMaxCargo, 
			  vehicleMaxVolume);
		
		this.vehicleModel = vehicleModel;
		this.vehicleSpeed  = 0.0;
		this.vehicleState = VehicleState.FREE;
		this.vehicleRegistrationDate = vehicleRegistrationDate;
		
	}
	
	protected void recalculateTotalCargo() {
		
	}
	
	protected void recalculateTotalVolume() {
		
	}
	
	/*SETTERS*/
	public void setVechicleSpeed(Double vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}
	public void setVichecleState(VehicleState vehicleState) {
		this.vehicleState = vehicleState;
	}
	/*END SETTERS*/
	/*GETTERS*/
	public String getVehicleModel() {
		
		return this.vehicleModel;
		
	}
	public Double getVehicleSpeed() {
		
		return this.vehicleSpeed;
		
	}
	public VehicleState getVehicleState() {
		
		return this.vehicleState;
		
	}
	public Date getVehicleRegistrationDate() {
		
		return this.vehicleRegistrationDate;
		
	}
	/*END GETTERS*/
	//SETTERS
	public void setVehicleLocation(Double vehicleCurrentPointX, Double vehicleCurrentPointY) {
		super.locationPointX = vehicleCurrentPointX;
		super.locationPointY = vehicleCurrentPointY;
	}
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)  
	@JoinTable(name = "VEHICLE_ORDER",
				joinColumns={
				@JoinColumn(name = "id_vehicle")},
				inverseJoinColumns={
				@JoinColumn(name ="id_order")})
	private Set<Order> orders;
	public Set<Order> getOrders() {

		return this.orders;

	}
	public void setOrders(Set<Order> orders) {
		
		this.orders = orders;
		
	}
}

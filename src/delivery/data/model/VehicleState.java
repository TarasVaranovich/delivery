package delivery.data.model;

public enum VehicleState {
	
	BUZY, FREE, SERVISING;
	
	public Boolean isAvailable() {
		if((this == FREE) || (this == BUZY)) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
}

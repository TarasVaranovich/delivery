package delivery.data.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import delivery.data.model.Goods; 


@Entity
@Table(name="warehouse")
public class Warehouse extends GoodsContainer{

	public Warehouse() {
		super(0.0, 0.0, null, 0.0, 0.0);
	}
	
	public Warehouse(Double warehouseLocationPointX,
			  Double warehouseLocationPointY,
			  Date warehouseBuildingdate, 
			  Double warehouseMaxCargo, 
			  Double warehouseMaxVolume) {
		
		super(warehouseLocationPointX,
			  warehouseLocationPointY,
			  warehouseBuildingdate, 
			  warehouseMaxCargo, 
			  warehouseMaxVolume);
		
	}
	
	protected void recalculateTotalCargo() {
		
	}
	
	protected void recalculateTotalVolume() {
		
	}
	 
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinTable(name = "WAREHOUSE_GOODS", 
	   joinColumns = @JoinColumn(name = "container_id"), 
	   inverseJoinColumns = @JoinColumn(name = "goods_id"))
	private Set<Goods> goods;
	public Set<Goods> getGoods() {
		
		return this.goods;
		
	}
	
	public void setGoods(Set<Goods> goods) {
		
		this.goods = goods;
		
	}
}

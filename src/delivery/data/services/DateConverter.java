package delivery.data.services;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {
	
	public Date convertFromString(String dateString) {
		
		try {
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date resultDate = simpleDateFormat.parse(dateString);		
			
			return resultDate;
		
		} catch(Exception e) {
			
			e.printStackTrace();
			
			return null;
			
		}
		
	}
	
	public Date getCurrentDate() {
		
		Date currentDate = new Date();
		
		return currentDate;
	}
	
}

package delivery.data.services;

import delivery.data.model.Goods;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface GoodsService {

	//GOODS CAN'T EXIST WITHOUT WAREHOUSE
	//FOR TEST ONLY
	boolean addGoods(String goodsName, Double goodsCargo, Double goodsVolume, Date goodsExpirationDate);
	boolean deleteGoods(Object identity);
	List<Goods> getAllGoods();
	List<Goods> getGoodsPage(Integer pageNumber, Integer pageSize);
	Set<Goods> getGoodsByIdsRange(Set<Integer> goodsIds);
}

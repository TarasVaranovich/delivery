package delivery.data.services;

import delivery.data.jpa.GoodsRepository;
import delivery.data.model.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Service
@Scope //spring bean scope(application, prototype)
@Transactional
public class GoodsServiceImpl implements GoodsService {

    private final GoodsRepository goodsRepository;

    @Autowired(required = true)
    public  GoodsServiceImpl(GoodsRepository goodsRepository){

        this.goodsRepository = goodsRepository;

    }

    //GOODS CAN'T EXIST WITHOUT WAREHOUSE
    //FOR TEST ONLY
    @Override
    public boolean addGoods(String goodsName, Double goodsCargo, Double goodsVolume, Date goodsExpirationDate) {

        Goods goods = new Goods(goodsName, goodsCargo, goodsVolume, goodsExpirationDate);
        try {

            goodsRepository.save(goods);
            return true;

        } catch (Exception e) {

            return false;

        }
    }


    @Override
    public boolean deleteGoods(Object identity) {

        try {

            goodsRepository.deleteById((Integer) identity);

            return true;

        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public List<Goods> getAllGoods() {

        List<Goods> goodsSelected = new ArrayList<>();
        for (Goods goo: goodsRepository.findAll()) {
            goodsSelected.add(goo);
        }

        return goodsSelected;
    }

    @Override
    public List<Goods> getGoodsPage(Integer pageNumber, Integer pageSize) {
        Pageable pageable = new PageRequest(pageNumber,pageSize);
        List<Goods> goodsSelected = new ArrayList<>();
        for (Goods goo: goodsRepository.findAll(pageable)) {

            goodsSelected.add(goo);

        }

        return goodsSelected;
    }

    @Override
    public Set<Goods> getGoodsByIdsRange(Set<Integer> goodsIds){

        return goodsRepository.findAllByIdIn(goodsIds);

    }
}

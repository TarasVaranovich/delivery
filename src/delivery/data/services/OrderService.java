package delivery.data.services;

import java.util.Date;
import java.util.List;

//import com.sun.tools.corba.se.idl.constExpr.Or;
import delivery.data.model.Goods;
import delivery.data.model.Order;
import delivery.data.model.OrderState;
import delivery.data.model.Vehicle;

public interface OrderService {
	
	boolean addOrder(Double orderLocartionX, Double orderLocationY, Date orderDate, OrderState orderState);
	boolean setOrderState(OrderState orderState, Object identity);
	List<Order> getAllOrders();
	boolean setOrderGoods();
	List<Goods> getOrderGoods(Object identity);
	Order getOrderById(Object identity);
	Order getActuallyOrder();
	List<Order> getVehiclePreformingOrders(Vehicle vehicle, OrderState orderState);
	List<Order> getOrdersPageByPersonEmail(Integer pageNumber, Integer pageSize, String personEmail);
	List<Order> gettOrdersRange(Integer pageNumber, Integer pageSize);
	List<Order> getVehicleOrdersPage(Vehicle vehicle, Integer pageNumber, Integer pageSize);
}

package delivery.data.services;


import delivery.data.jpa.OrderRepository;
import delivery.data.model.Goods;
import delivery.data.model.Order;
import delivery.data.model.OrderState;
import delivery.data.model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Service
@Scope //spring bean scope(application, prototype)
@Transactional
public class OrderServiceImpl implements OrderService{

    private final OrderRepository orderRepository;

    @Autowired(required = true)
    public OrderServiceImpl(OrderRepository orderRepository) {

        this.orderRepository = orderRepository;
    }

    @Override
    public boolean addOrder(Double orderLocartionX, Double orderLocationY, Date orderDate, OrderState orderState) {

        Order order;
        order = new Order(orderLocartionX,orderLocationY, orderDate, orderState);
        final Order save;
        save = orderRepository.save(order);

        return false;
    }

    @Override
    public boolean setOrderState(OrderState orderState, Object identity) {

        Order orderSelected = orderRepository.findById((Integer)identity);

        try {

            orderSelected.setState(orderState);

            return true;

        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public List<Order> getAllOrders() {

        List<Order> orders = new ArrayList<>();
        for(Order ord: orderRepository.findAll()) {
            orders.add(ord);
        }

        return orders;
    }

    @Override
    public boolean setOrderGoods(){

        //MUST BE DECLARE IN INTERFACE AND REALIZED HIER
        return true;
    }

    @Override
    public List<Goods> getOrderGoods(Object identity) {

        List<Goods> orderGoods = new ArrayList<>();
        //MUST BE DECLARE IN INTERFACE AND REALIZED HIER

        return  orderGoods;
    }

    @Override
    public Order getOrderById(Object identity){

        return orderRepository.findById((Integer)identity);

    }

    @Override
    public Order getActuallyOrder(){

        return orderRepository.findTopByOrderStateOrderByOrderDateAsc(OrderState.WAIT);
    }

    @Override
    public  List<Order> getVehiclePreformingOrders(Vehicle vehicle, OrderState orderState) {
        return  orderRepository.findAllByVehiclesAndOrderState(vehicle, orderState);
    }

    @Override
    public List<Order> getOrdersPageByPersonEmail(Integer pageNumber, Integer pageSize, String personEmail) {

        List<Order> resultOrders = new ArrayList<Order>();
        Pageable pageable = new PageRequest(pageNumber, pageSize);

        for (Order ord: orderRepository.getPersonOrders(personEmail, pageSize * pageNumber, pageSize)) {

            resultOrders.add(ord);

        }

        return resultOrders;
    }

    @Override
    public List<Order> gettOrdersRange(Integer pageNumber, Integer pageSize) {
        Pageable pageable = new PageRequest(pageNumber,pageSize);
        List<Order> ordersSelected = new ArrayList<Order>();

        for (Order ord: orderRepository.findAll(pageable)){
            ordersSelected.add(ord);
        }

        return ordersSelected;
    }

    @Override
    public List<Order> getVehicleOrdersPage(Vehicle vehicle, Integer pageNumber, Integer pageSize){
        List<Order> ordersSelected = new ArrayList<Order>();
        Pageable pageable = new PageRequest(pageNumber,pageSize);

        for (Order ord: orderRepository.findAllByVehicles(vehicle, pageable)) {
            ordersSelected.add(ord);
        }
        return ordersSelected;
    }
}

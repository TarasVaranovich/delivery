package delivery.data.services;

import java.util.List;
import java.util.Set;

//import com.sun.tools.corba.se.idl.constExpr.Or;
import delivery.data.model.Person;
import delivery.data.model.Order;

public interface PersonService {
	
	boolean addPerson(String personName, 
			          String personSurname, 
			          String personPhoneNumber, 
			          String personEmail);
	
	List<?> getAllPersons();
	
	boolean setPersonOrders(Set<Order> personOrders, Object parameter); 
	
	Set<?> getPersonOrders(Object parameter);

	Person getPersonByEmail(String personEmail);
	List<Person> getPersonsPage(Integer pageNumber, Integer pageSize);

}

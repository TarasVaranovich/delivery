package delivery.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.PersonRepository;
import delivery.data.model.Person;
import delivery.data.model.Order;

import java.util.*;

@Service
@Scope //spring bean scope(application, prototype)
@Transactional
public class PersonServiceImpl implements PersonService{
	
	private final PersonRepository personRepository;
	
	@Autowired(required=true)
	public PersonServiceImpl(PersonRepository personRepository) {
		
		this.personRepository = personRepository;
		
	}
	
	public boolean addPerson(String personName, 
	          	String personSurname, 
	          	String personPhoneNumber, 
	          	String personEmail) {
		
		Person person = new Person(personName, personSurname, personPhoneNumber, personEmail);
		
		try {
			
			personRepository.save(person);
			
			return true;
			
		} catch (Exception e) {
			
			return false;
			
		} 
		
		
	}
	
	public List<Person> getAllPersons() {
		
		List<Person> persons = new ArrayList();
		for(Person person: personRepository.findAll()) {
			
			persons.add(person);
			
		}
		
		return persons;
	}
	
	public boolean setPersonOrders(Set<Order> personOrders, Object email) {
		
		Person personSelected = personRepository.findBypersonEmail(email.toString());
		
		try {
			
			personSelected.setOrders((SortedSet) personOrders);
			
			return true;
			
		} catch(Exception e) {
			
			return false;
			
		}		
	}
	
	public Set<Order> getPersonOrders(Object email) {
		
		Set<Order> personOrders = new HashSet();
		Person personSelected = personRepository.findBypersonEmail(email.toString());
		personOrders = (Set)personSelected.getOrders();
		
		return personOrders;
	}

	public Person getPersonByEmail(String personEmail) {
		return personRepository.findBypersonEmail(personEmail);
	}

	@Override
	public List<Person> getPersonsPage(Integer pageNumber, Integer pageSize){

		Pageable pageable = new PageRequest(pageNumber,pageSize);

		List<Person> personsSelected = new ArrayList<Person>();

		for(Person pers: personRepository.findAll(pageable)){

			personsSelected.add(pers);

		}

		return personsSelected;
	}

}

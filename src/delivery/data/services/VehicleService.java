package delivery.data.services;

import java.util.Date;
import java.util.List;

import delivery.data.model.Order;
import delivery.data.model.Vehicle;
import delivery.data.model.VehicleState;

public interface VehicleService {
	
	boolean addVehicle(Date vehicleProductionDate,
                       Double vehicleMaxCargo,
                       Double vehicleMaxVolume,
                       Double vehicleTotalCargo,
                       String vehicleModel,
                       Date vehicleRegictrationDate,
                       Double vehicleCurrentPointX,
                       Double vehicleCurrentPointY);
	
	boolean setVehicleState(VehicleState vehicleState, Object identity);
	boolean setVehicleLocation(Double vehicleLocationX, Double vehicleLocationY, Object identity);
	List<Vehicle> getAllVehicles();
	List<Order> getVehicleOrders();
	Vehicle getVehicleByIdentity(Object identity);
}

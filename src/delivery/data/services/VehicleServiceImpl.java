package delivery.data.services;

import delivery.data.jpa.VehicleRepository;
import delivery.data.model.Order;
import delivery.data.model.Vehicle;
import delivery.data.model.VehicleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Scope//spring bean scope(application, prototype)
@Transactional
public class VehicleServiceImpl implements VehicleService{

    private final VehicleRepository vehicleRepository;

    @Autowired(required = true)
    public  VehicleServiceImpl(VehicleRepository vehicleRepository) {

        this.vehicleRepository = vehicleRepository;

    }

    @Override
    public boolean addVehicle(Date vehicleProductionDate,
                              Double vehicleMaxCargo,
                              Double vehicleMaxVolume,
                              Double vehicleTotalCargo,
                              String vehicleModel,
                              Date vehicleRegictrationDate,
                              Double vehicleCurrentPointX,
                              Double vehicleCurrentPointY) {

        Vehicle vehicle = new Vehicle(vehicleModel,
                                        vehicleRegictrationDate,
                                        vehicleProductionDate,
                                        vehicleMaxCargo,
                                        vehicleMaxVolume,
                                        vehicleCurrentPointX,
                                        vehicleCurrentPointY);

        final Vehicle save;
        save = vehicleRepository.save(vehicle);

        return true;
    }

    @Override
    public boolean setVehicleState(VehicleState vehicleState, Object identity) {

        Vehicle vehicleSelected = vehicleRepository.findById((Integer) identity);
        try {

            vehicleSelected.setVichecleState(vehicleState);
            return true;

        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public boolean setVehicleLocation(Double vehicleLocationX, Double vehicleLocationY, Object identity){

        Vehicle vehicleSelected = vehicleRepository.findById((Integer) identity);
        try {

            vehicleSelected.setVehicleLocation(vehicleLocationX, vehicleLocationY);

            return true;

        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public List<Vehicle> getAllVehicles() {

        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        for(Vehicle veh: vehicleRepository.findAll()) {
            vehicles.add(veh);
        }

        return vehicles;
    }

    @Override
    public List<Order> getVehicleOrders() {

        List<Order> vehicleOrders = new ArrayList<Order>();

        //MUST BE DECLARE IN INTERFACE AND REALIZED HIER

        return vehicleOrders;
    }

    @Override
    public Vehicle getVehicleByIdentity(Object identity){

        return vehicleRepository.findById((Integer)identity);

    }
}

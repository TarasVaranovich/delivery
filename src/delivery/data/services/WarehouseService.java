package delivery.data.services;

import delivery.data.model.Goods;
import delivery.data.model.Warehouse;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface WarehouseService {
	
	boolean addWarehouse(Double warehouseLocationX, 
			          Double warehouseLocationY, 
			          Date warehouseCreationDate, 
			          Double warehouseMaxCargo, 
			          Double maxVolume);
	List<Warehouse> getWarehouses();
	boolean addGoods(Set<Goods> goodsSet, Object identity);
	Set<Goods> getGoods(Object identity);
	Warehouse getWarehouseById(Integer warehouseId);
}

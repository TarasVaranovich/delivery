package delivery.data.services;

import delivery.data.jpa.WarehouseRepository;
import delivery.data.model.Goods;
import delivery.data.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Scope //spring bean scope(application, prototype)
@Transactional
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;

    @Autowired(required = true)
    WarehouseServiceImpl(WarehouseRepository warehouseRepository) {

        this.warehouseRepository = warehouseRepository;

    }

    @Override
    public boolean addWarehouse(Double warehouseLocationX,
                                Double warehouseLocationY,
                                Date warehouseCreationDate,
                                Double warehouseMaxCargo,
                                Double maxVolume) {
        Warehouse warehouse = new Warehouse(warehouseLocationX, warehouseLocationY, warehouseCreationDate, warehouseMaxCargo, maxVolume);

        try {
            warehouseRepository.save(warehouse);

            return true;

        } catch (Exception e) {

            return false;

        }
    }

    @Override
    public List<Warehouse> getWarehouses() {
        List<Warehouse> warehouses = new ArrayList<>();
        for(Warehouse wh: warehouseRepository.findAll()) {
            warehouses.add(wh);
        }

        return warehouses;
    }

    @Override
    public  boolean addGoods(Set<Goods> goods, Object identity){

        try {

            Warehouse warehouse = warehouseRepository.findById((Integer) identity);
            warehouse.setGoods(goods);

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Set<Goods> getGoods(Object identity) {

        Set<Goods> warehouseGoods = new HashSet<>();
        Warehouse warehouse = warehouseRepository.findById((Integer) identity);
        warehouseGoods = warehouse.getGoods();

        return warehouseGoods;
    }

    @Override
    public Warehouse getWarehouseById(Integer warehouseId){

        return warehouseRepository.findById(warehouseId);

    }
}

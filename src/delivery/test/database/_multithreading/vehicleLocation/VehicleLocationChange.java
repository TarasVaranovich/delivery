package delivery.test.database._multithreading.vehicleLocation;

import delivery.config.PersistenceJPAConfig;
import delivery.data.services.VehicleService;
import delivery.config.TestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PersistenceJPAConfig.class, TestConfig.class}, loader = AnnotationConfigContextLoader.class)
public class VehicleLocationChange {

    @Autowired
    VehicleService vehicleService;

    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Test
    //@Transactional
    public void vehicleLocationReadWrite(){
        /*VehicleSetLocation vsl = new VehicleSetLocation("Model", vehicleService); // performing with delay
        vsl.run();*/

        /*Thread vslThread = new Thread(new VehicleSetLocation());
        vslThread.start();*/
        /*ExecutorService exec = Executors.newCachedThreadPool(); // ASYNC
        for(int i = 0; i < 5; i++) {
            System.out.println("Start thread " + i + " execution.");
            exec.execute(new VehicleSetLocation("Model" + i, vehicleService));
            //exec.shutdown();
        }*/

        /*ExecutorService exec = Executors.newSingleThreadExecutor(); // SYNC
        for(int i = 0; i < 5; i++) {
            exec.execute(new VehicleSetLocation("Model" + i, vehicleService));
            //exec.shutdown();
        }*/
        //VehicleSetLocation vcl = new VehicleSetLocation("Model", vehicleService);
        //vcl.run();
        /*threadPoolTaskExecutor.initialize();
        threadPoolTaskExecutor.execute(new VehicleSetLocation("Model", vehicleService));*/

        threadPoolTaskExecutor.initialize();
        for(int i = 0 ;i < 5; i ++) {
            threadPoolTaskExecutor.execute(new VehicleSetLocation("Model" + i, vehicleService));
            //try {Thread.sleep(1000);} catch (Exception e) {}
        }

    }
}

package delivery.test.database._multithreading.vehicleLocation;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Vehicle;
import delivery.data.services.DateConverter;
import delivery.data.services.VehicleService;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Random;


public class VehicleSetLocation implements Runnable{

    private Integer vehicleID;
    private static Integer vehicleIDnext = 1;
    private String vehicleModel;
    private static VehicleService vehicleService;


    public VehicleSetLocation(String vehicleModel ,VehicleService vehicleService) {
        if(this.vehicleService == null) {
            this.vehicleService = vehicleService;
        }
        this.vehicleModel = vehicleModel;
        this.vehicleID = vehicleIDnext;
        vehicleIDnext = vehicleIDnext + 1;
        this.createVehicle();
    }

    private void createVehicle() {
        DateConverter dc = new DateConverter();
        Date dateTest = dc.getCurrentDate();
        try {
           this.vehicleService.addVehicle(dateTest, 12.87, 22.22, 0.0, this.vehicleModel, dateTest, 100.01, 100.02);
           System.out.println("Added vehicles:");
            for(Vehicle veh:vehicleService.getAllVehicles()) {
                System.out.println("ID:" + veh.getId());
            }
        } catch (Exception e) {
            System.out.println("Exception " + vehicleModel + " creation:");
            e.printStackTrace();
        }
    }

    @Transactional
    public void run() {

        for (int i = 0; i < 5; i++) {

            this.updateLocation();
            this.getVehicleInfo();

            try {

               //Thread.sleep(1000); /*because of delay not perform all requests*/

            }catch (Exception e) {

                e.printStackTrace();

            }
        }

        Thread.yield();
    }


    private void updateLocation() {

        this.vehicleService.setVehicleLocation(this.generateRandomDouble(), this.generateRandomDouble(), this.vehicleID);

    }

    private void getVehicleInfo() {

        Vehicle vehicleInfo = vehicleService.getVehicleByIdentity(this.vehicleID);

        System.out.println("(hashcode: " +
                            System.identityHashCode(this) + ") call-" + ":ID-" +
                            this.vehicleID + ": Model:" +
                            vehicleInfo.getVehicleModel() + ":locations:" +
                            vehicleInfo.getLocationPointX() + "|" +
                            vehicleInfo.getLocationPointY());
    }

    private double generateRandomDouble() {

        final double rangeMin = 0;
        final double rangeMax = 100;
        Random random = new Random();

        return rangeMin + (rangeMax - rangeMin) * random.nextDouble();
    }
}

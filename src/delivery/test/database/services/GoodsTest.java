package delivery.test.database.services;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Goods;
import delivery.data.services.DateConverter;
import delivery.data.services.GoodsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class GoodsTest {

    @Autowired
    GoodsService goodsService;

    @Test
    @Transactional
    public void goodsTest() {

        System.out.println("Goods test...");

        DateConverter dateConverter = new DateConverter();
        Date testDate = dateConverter.getCurrentDate();

        //add goods
        goodsService.addGoods("goodsOne", 0.1, 0.1, testDate);
        goodsService.addGoods("goodsTwo", 0.2, 0.2, testDate);
        goodsService.addGoods("goodsThree", 0.3, 0.3, testDate);
        goodsService.addGoods("goodsFour", 0.4, 0.4, testDate);
        goodsService.addGoods("goodsFive", 0.5, 0.5, testDate);
        goodsService.addGoods("goodsSix", 0.6, 0.6, testDate);

        //get all goods
        getAllGoods();

        //get goods page
        System.out.println("Goods page...");
        List<Goods> goodsListPage = new ArrayList<>();
        goodsListPage = goodsService.getGoodsPage(2,2);
        for(Goods goo: goodsListPage) {
            System.out.println(goo.getId() + ":" + goo.getName() + ":" + goo.getCargo() + ":" + goo.getVolume() + goo.getExpirationDate());
        }

        //delete goods by id
        goodsService.deleteGoods(2);

        //get all goods after deleting
        System.out.println("All goods after deleting...");
        getAllGoods();

        //get goods by ids's Range
        System.out.println("Getting goods by id's range...");
        Set<Integer> goodsIds = new HashSet<Integer>();
        goodsIds.add(1);
        goodsIds.add(5);
        goodsIds.add(3);
        Set<Goods> goodsRange = new HashSet<Goods>();
        goodsRange = goodsService.getGoodsByIdsRange(goodsIds);
        for(Goods goo: goodsRange) {
            System.out.println(goo.getId() + ":" + goo.getName() + ":" + goo.getCargo() + ":" + goo.getVolume() + goo.getExpirationDate());
        }

    }

    private void getAllGoods() {
        List<Goods> goodsList = new ArrayList<>();
        goodsList = goodsService.getAllGoods();
        for (Goods goo: goodsList) {
            System.out.println(goo.getId() + ":" + goo.getName() + ":" + goo.getCargo() + ":" + goo.getVolume() + goo.getExpirationDate());
        }
    }
}

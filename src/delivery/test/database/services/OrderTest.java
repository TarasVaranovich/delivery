package delivery.test.database.services;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Order;
import delivery.data.services.OrderService;
import delivery.data.model.OrderState;
import delivery.data.services.DateConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class OrderTest {

    @Autowired
    private OrderService orderService;

    @Test
    @Transactional
    public void orderTest(){

        System.out.println("Order test...");

        //add orders
        DateConverter dateConverter = new DateConverter();
        Date dateTest = dateConverter.getCurrentDate();
        Date dateOne = dateConverter.convertFromString("24/11/2017 09:00:45");
        Date dateTwo = dateConverter.convertFromString("22/11/2017 09:00:45");
        Date dateThree = dateConverter.convertFromString("23/11/2017 09:00:45");
        orderService.addOrder(0.23, 0.24, dateOne, OrderState.WAIT);
        orderService.addOrder(1.12, 234.98, dateTwo, OrderState.WAIT);
        orderService.addOrder(44.25, 45.98, dateThree, OrderState.WAIT);
        orderService.addOrder(0.25, 0.44, dateTest, OrderState.COMPLETED);
        orderService.addOrder(0.55, 0.74, dateTest, OrderState.PERFORMING);
        orderService.addOrder(0.82, 0.98, dateTest, OrderState.PERFORMING);

        //get orders
        getAllOrders();

        //set order state
        Integer orderId = 6;
        orderService.setOrderState(OrderState.COMPLETED, orderId);

        //get all orders (check is id setted)
        System.out.println("After state changed:");
        getAllOrders();

        //get order by id
        System.out.println("Getting order by Id...");
        Order orderSelected = orderService.getOrderById(5);
        System.out.println(orderSelected.getLocationPointX() + ":" +
                orderSelected.getLocationPointY() + ":" +
                orderSelected.getDate() + ":" +
                orderSelected.getState() + ":ID:" +
                orderSelected.getId());

        //get next actually order
        System.out.println("Getting most actually order (earlier date)...");
        Order actuallyOrder = orderService.getActuallyOrder();
        if(actuallyOrder != null) {
            System.out.println(actuallyOrder.getLocationPointX() + ":" +
                    actuallyOrder.getLocationPointY() + ":" +
                    actuallyOrder.getDate() + ":" +
                    actuallyOrder.getState() + ":ID:" +
                    actuallyOrder.getId());
        } else {
            System.out.println("Ain't no actually orders in database...");
        }

    }

    private void getAllOrders() {
        List<Order> resultOrders = orderService.getAllOrders();
        for (Order ord: resultOrders) {
            System.out.println(ord.getLocationPointX() + ":" +
                    ord.getLocationPointY() + ":" +
                    ord.getDate() + ":" +
                    ord.getState() + ":ID:" +
                    ord.getId());
        }
    }
}

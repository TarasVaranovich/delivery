package delivery.test.database.services;

import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.services.PersonService;
import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Person;
import delivery.data.model.Order;
import delivery.data.model.OrderState;
import delivery.data.services.DateConverter;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;



@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class PersonTest {
	
	@Autowired
	private PersonService personService;
	
	@Test
	@Transactional
	public void personTest(){
		
		System.out.println("Person test...");
		
		// add persons
		personService.addPerson("P1F", "P1L", "P1+23", "P1@mail");
		personService.addPerson("P2F", "P2L", "P2+23", "P2@mail");		
		
		//get persons
		List<Person> persons = new ArrayList();
		persons = (List<Person>) personService.getAllPersons();
		for(Person pers: persons) {
			System.out.println(pers.getName() + ":" + 
							   pers.getSurname() + ":" + 
							   pers.getPhoneNumber() + ":" + 
							   pers.getEmail());
		}	
		//set person orders
		DateConverter dateConverter = new DateConverter();
		Date dateTest = dateConverter.getCurrentDate();
		Set ordersTestPerson = new HashSet<Order>(){{
            add(new Order(211.1, 221.2, dateTest, OrderState.WAIT));
            add(new Order(212.1, 222.2, dateTest, OrderState.WAIT));
            add(new Order(213.1, 223.2, dateTest, OrderState.WAIT));
        }};
        personService.setPersonOrders(ordersTestPerson, "P1@mail");
        //get person orders
        Set<Order> personOrdersResult = new HashSet();
        personOrdersResult = (Set<Order>) personService.getPersonOrders("P1@mail");
        System.out.println("Orders count:" + personOrdersResult.size());
        for(Order ord: personOrdersResult) {
			System.out.println(ord.getState() + ":" + 
							   ord.getDate() + ":" + 
							   ord.getLocationPointX() + ":" + 
							   ord.getLocationPointY());
		}	
	}
}

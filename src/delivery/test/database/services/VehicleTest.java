package delivery.test.database.services;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Vehicle;
import delivery.data.model.VehicleState;
import delivery.data.services.DateConverter;
import delivery.data.services.VehicleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class VehicleTest {

    @Autowired
    VehicleService vehicleService;

    @Test
    @Transactional
    public void vehicleTest() {

        System.out.println("Vehicle test...");

        //add vehicles
        DateConverter dateConverter = new DateConverter();
        Date dateTest = dateConverter.getCurrentDate();
        vehicleService.addVehicle(dateTest, 23.44, 0.23, 0.45, "Vehicle One", dateTest, 144.1, 345.67);
        vehicleService.addVehicle(dateTest, 23.44, 0.23, 0.45, "Vehicle Two", dateTest, 120.1, 200.67);
        vehicleService.addVehicle(dateTest, 23.44, 0.20, 0.45, "Vehicle Three", dateTest, 500.1, 700.67);

        //get vehicles
        getAllVehicles();

        //set vehicle state
        Integer vehId = 1;
        Integer vehId2 = 2;
        vehicleService.setVehicleState(VehicleState.BUZY,vehId);
        vehicleService.setVehicleState(VehicleState.SERVISING, vehId2);
        vehicleService.setVehicleLocation(22.34, 24.08, vehId);
        vehicleService.setVehicleLocation(44.5, 56.89, vehId2);
        //get all vehicles (checking of changes)
        System.out.println("After setting vehicle states...");
        getAllVehicles();
        //get vehicle by id
        Vehicle vehicleById = vehicleService.getVehicleByIdentity(2);
        System.out.println("Vehicle by idenitity...");
        System.out.println(vehicleById.getId() + ":" + vehicleById.getVehicleModel());

    }

    private void getAllVehicles() {
        List<Vehicle> vehiclesResult = vehicleService.getAllVehicles();
        for (Vehicle veh: vehiclesResult) {
            System.out.println(veh.getId() + ":" +
                    veh.getVehicleModel() + ":" +
                    veh.getVehicleState() + ":" +
                    veh.getVehicleSpeed() + ":" +
                    veh.getVehicleRegistrationDate() + ":" +
                    veh.getCreationDate() + ":" +
                    veh.getLocationPointX() + ":" +
                    veh.getLocationPointY() + ":" +
                    veh.getMaxCargo() + ":" +
                    veh.getTotalCargo() + ":" +
                    veh.getMaxVolume() + ":" +
                    veh.getTotalVolume());
        }
    }
}

package delivery.test.database.services;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.Goods;
import delivery.data.model.Warehouse;
import delivery.data.services.DateConverter;
import delivery.data.services.WarehouseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class WarehouseTest {

    @Autowired
    WarehouseService warehouseService;

    @Test
    @Transactional
    public void warehouseTest() {

        System.out.println("Warehouse test...");

        //add warehouse
        DateConverter dateConverter = new DateConverter();
        Date dateTest = dateConverter.getCurrentDate();

        warehouseService.addWarehouse(130.56, 80.78, dateTest, 0.56, 0.89);
        warehouseService.addWarehouse(560.89, 90.67, dateTest, 0.24, 0.89);
        warehouseService.addWarehouse(860.89, 980.67, dateTest, 0.28, 0.002);

        //get warehouses
        List<Warehouse> warehousesResult = new ArrayList();
        warehousesResult = warehouseService.getWarehouses();
        for (Warehouse war: warehousesResult) {
            System.out.println(war.getId() + ":" +
                    war.getCreationDate() + ":" +
                    war.getLocationPointX() + ":" +
                    war.getLocationPointY() + ":" +
                    war.getMaxCargo() + ":" +
                    war.getMaxVolume());
        }

        //set warehouse goods
        Integer warehouseId = 1;
        Set<Goods> goodsSet = new HashSet<Goods>(){{

                add(new Goods("goodsOne", 1.23, 0.02, dateTest));
                add(new Goods("goodsTwo", 1.23, 0.02, dateTest));
                add(new Goods("goodsThree", 1.23, 0.02, dateTest));
            }};
        warehouseService.addGoods(goodsSet, warehouseId);

        //get warehouse gooods
        Set<Goods> warehouseGoodsSelected = new HashSet();
        warehouseGoodsSelected = warehouseService.getGoods(warehouseId);
        for (Goods goo: warehouseGoodsSelected) {
            System.out.println(goo.getId() + ":" +
                                goo.getName() + ":" +
                                goo.getExpirationDate() + ":" +
                                goo.getCargo() + ":" +
                                goo.getVolume());
        }

    }

}

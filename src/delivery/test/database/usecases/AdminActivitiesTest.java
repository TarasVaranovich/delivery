package delivery.test.database.usecases;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.*;
import delivery.data.services.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class AdminActivitiesTest {

    @Autowired
    WarehouseService warehouseService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    OrderService orderService;
    @Autowired
    VehicleService vehicleService;
    @Autowired
    PersonService personService;

    @Test
    @Transactional
    public void adminActivitiesTest() {
        //add warehouse test
        System.out.println("Add warehouse test...");
        DateConverter dc = new DateConverter();
        Date warehouseBuildingDate = dc.convertFromString("01/01/2008 18:00:00");
        addWarehouse(134.56,148.245, warehouseBuildingDate, 120.0, 1000.25);
        List<Warehouse> allWarehouses = new ArrayList<Warehouse>();
        allWarehouses = warehouseService.getWarehouses();
        for (Warehouse ware: allWarehouses) {
            System.out.println(ware.getId() + "|" +
                                ware.getCreationDate() + "|" +
                                ware.getLocationPointX() + "|" +
                                ware.getLocationPointY() + "|" +
                                ware.getMaxCargo() + "|" +
                                ware.getMaxVolume());
        }
        //add goods test
        System.out.println("Add goods test...");
        Set<Goods> warehouseGoods = new HashSet<Goods>();
        warehouseGoods.add(new Goods("goodsOne", 0.1, 0.1, dc.getCurrentDate()));
        warehouseGoods.add(new Goods("goodsTwo", 0.2, 0.2, dc.getCurrentDate()));
        warehouseGoods.add(new Goods("goodsThree", 0.3, 0.3, dc.getCurrentDate()));
        warehouseGoods.add(new Goods("goodsFour", 0.4, 0.4, dc.getCurrentDate()));
        addWarehouseGoods(1,warehouseGoods);
        Warehouse warehouseGoodsAddingOne = warehouseService.getWarehouseById(1);
        for(Goods goo: warehouseGoodsAddingOne.getGoods()){
            System.out.println(goo.getId() + "|" + goo.getName());
        }
        System.out.println("Add goods second test...");
        Set<Goods> secondGoodsSet = new HashSet<Goods>();
        secondGoodsSet.add(new Goods("goodsFive", 0.5, 0.5, dc.getCurrentDate()));
        secondGoodsSet.add(new Goods("goodsSix", 0.6, 0.6, dc.getCurrentDate()));
        addWarehouseGoods(1, secondGoodsSet);
        Warehouse warehouseGoodsAddingTwo = warehouseService.getWarehouseById(1);
        for(Goods goo: warehouseGoodsAddingTwo.getGoods()){
            System.out.println(goo.getId() + "|" + goo.getName());
        }
        System.out.println("See all users test");
        fillDataPersons();
        for(int i = 0; i < 6 ;i++) {
            try {
                if(seeAllUsers(i,2).size() > 0) {
                    System.out.println("--page-" + i + "--");
                    for (Person pers : seeAllUsers(i, 2)) {

                        System.out.println(pers.getEmail() + ":" +
                                pers.getName() + ":" +
                                pers.getSurname() + ":" +
                                pers.getPhoneNumber());
                    }
                } else {

                    break;

                }
                Thread.sleep(300);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("See user orders test...");
        for(int i = 0; i < 6; i++ ){
            try {
                if(seeUserOrders(i,2,"P2@mail").size() > 0) {
                    System.out.println("--page-" + i + "--");
                    for (Order ord: seeUserOrders(i,2,"P2@mail")) {

                        System.out.println(ord.getId() + "|" +
                                            ord.getLocationPointX() + "|" +
                                            ord.getLocationPointY() + "|" +
                                            ord.getDate() + "|" +
                                            ord.getState());
                    }
                } else {

                    break;

                }
                Thread.sleep(300);

            } catch (Exception e) {

                e.printStackTrace();

            }
        }
        System.out.println("See all orders test...");
        for(int i = 0; i < 12; i++ ){
            try {
                if(seeOrders(i,2).size() > 0) {
                    System.out.println("--page-" + i + "--");
                    for (Order ord: seeOrders(i,2)) {

                        System.out.println(ord.getId() + "|" +
                                ord.getLocationPointX() + "|" +
                                ord.getLocationPointY() + "|" +
                                ord.getDate() + "|" +
                                ord.getState());
                    }
                } else {

                    break;

                }
                Thread.sleep(300);

            } catch (Exception e) {

                e.printStackTrace();

            }
        }
        System.out.println("Add vehicle test...");
        DateConverter dCVeh = new DateConverter();
        Date vDate = dCVeh.getCurrentDate();
        addVehicle("VehicleOne",vDate,vDate,23.05,44.67,23.07, 456.08);
        addVehicle("VehicleTwo",vDate,vDate,67.05,134.67,231.07, 1456.089);
        for (Vehicle v: vehicleService.getAllVehicles()) {
            System.out.println("Vehicle added:" + v.getId() + v.getVehicleModel());
        }
        System.out.println("See vehicle locations...");
        for (int i = 0; i < 3; i++) {
            try{
                System.out.println("Vehicle 2 location:" + seeVehicleLocation(2)[0] + ":" + seeVehicleLocation(2)[1]);
                Thread.sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("See vehicle orders...");
        Vehicle vehicleOne = vehicleService.getVehicleByIdentity(2);
        Vehicle vehicleTwo = vehicleService.getVehicleByIdentity(3);
        List<Order> vehicleOneOrders = orderService.getOrdersPageByPersonEmail(1,2,"P1@mail");
        List<Order> vehicleTwoOrders = orderService.getOrdersPageByPersonEmail(2,2,"P2@mail");
        SortedSet<Order> setForAllOrders = new TreeSet<Order>();
        SortedSet<Order> setForAllOrdersTwo = new TreeSet<Order>();
        for (Order order: vehicleOneOrders) {
            setForAllOrders.add(order);
        }
        for (Order order: vehicleTwoOrders) {
            setForAllOrdersTwo.add(order);
        }
        vehicleOne.setOrders(setForAllOrders);
        vehicleTwo.setOrders(setForAllOrdersTwo);
        int vehicleID = 2; //3
        for(int i = 0; i < 12; i++ ){
            try {
                if(seeVehicleOrders(vehicleID, i,2).size() > 0) {
                    System.out.println("--page-" + i + "--");
                    for (Order ord: seeVehicleOrders(vehicleID, i,2)) {

                        System.out.println(ord.getId() + "|" +
                                ord.getLocationPointX() + "|" +
                                ord.getLocationPointY() + "|" +
                                ord.getDate() + "|" +
                                ord.getState());
                    }
                } else {

                    break;

                }
                Thread.sleep(300);

            } catch (Exception e) {

                e.printStackTrace();

            }
        }

    }
    //USE-CASE "ADD WAREHOUSE"
    private void addWarehouse(Double warehouseLocationPointX,
                             Double warehouseLocationPointY,
                             Date warehouseBuildingdate,
                             Double warehouseMaxCargo,
                             Double warehouseMaxVolume) {
        warehouseService.addWarehouse(warehouseLocationPointX,
                                        warehouseLocationPointY,
                                        warehouseBuildingdate,
                                        warehouseMaxCargo,
                                        warehouseMaxVolume );
    }
    //USE-CASE "ADD GOODS"
    private void addWarehouseGoods(Integer warehouseId, Set<Goods> warehouseGoods){
        Warehouse warehouseGoodsSetting = warehouseService.getWarehouseById(warehouseId);
        if(warehouseGoodsSetting.getGoods() == null) {
            warehouseGoodsSetting.setGoods(warehouseGoods);
        } else {
            warehouseGoodsSetting.getGoods().addAll(warehouseGoods);
        }
    }
    //USE-CASE "SEE USERS"
    private  List<Person> seeAllUsers(Integer pageNumber, Integer pageSize) {

        return personService.getPersonsPage(pageNumber, pageSize);

    }
    private void fillDataPersons() {
        DateConverter dateConverter = new DateConverter();

        personService.addPerson("P1F", "P1L", "P1+23", "P1@mail");
        Person personOne = personService.getPersonByEmail("P1@mail");
        SortedSet<Order> personOneOrders = new TreeSet<Order>();
        Date dateTest1 = dateConverter.convertFromString("29/11/2017 00:00:01");
        personOneOrders.add(new Order(0.11, 0.24, dateTest1, OrderState.WAIT));
        Date dateTest2 = dateConverter.convertFromString("30/11/2017 00:00:01");
        personOneOrders.add(new Order(0.12, 0.24, dateTest2, OrderState.COMPLETED));
        Date dateTest3 = dateConverter.convertFromString("30/11/2017 08:00:01");
        personOneOrders.add(new Order(0.13, 0.24, dateTest3, OrderState.PERFORMING));
        Date dateTest4 = dateConverter.convertFromString("30/11/2017 08:20:01");
        personOneOrders.add(new Order(0.14, 0.24, dateTest4, OrderState.WAIT));
        Date dateTest5 = dateConverter.convertFromString("30/11/2017 18:08:45");
        personOneOrders.add(new Order(0.15, 0.24, dateTest5, OrderState.COMPLETED));
        Date dateTest6 = dateConverter.convertFromString("30/11/2017 20:36:45");
        personOneOrders.add(new Order(0.16, 0.24, dateTest6, OrderState.PERFORMING));
        personOne.setOrders(personOneOrders);

        personService.addPerson("P2F", "P2L", "P2+23", "P2@mail");
        Person personTwo = personService.getPersonByEmail("P2@mail");
        SortedSet<Order> personTwoOrders = new TreeSet<Order>();
        Date dateTest7 = dateConverter.convertFromString("29/12/2017 00:00:01");
        personTwoOrders.add(new Order(0.17, 0.24, dateTest7, OrderState.WAIT));
        Date dateTest8 = dateConverter.convertFromString("30/12/2017 00:00:01");
        personTwoOrders.add(new Order(0.18, 0.24, dateTest8, OrderState.COMPLETED));
        Date dateTest9 = dateConverter.convertFromString("30/12/2017 08:00:01");
        personTwoOrders.add(new Order(0.19, 0.24, dateTest9, OrderState.PERFORMING));
        Date dateTest10 = dateConverter.convertFromString("30/12/2017 08:20:01");
        personTwoOrders.add(new Order(0.20, 0.24, dateTest10, OrderState.WAIT));
        Date dateTest11 = dateConverter.convertFromString("30/12/2017 18:08:45");
        personTwoOrders.add(new Order(0.21, 0.24, dateTest11, OrderState.COMPLETED));
        Date dateTest12 = dateConverter.convertFromString("30/12/2017 20:36:45");
        personTwoOrders.add(new Order(0.22, 0.24, dateTest12, OrderState.PERFORMING));
        personTwo.setOrders(personTwoOrders);

        personService.addPerson("P3F", "P3L", "P3+23", "P3@mail");
        personService.addPerson("P4F", "P4L", "P4+23", "P4@mail");
        personService.addPerson("P5F", "P5L", "P5+23", "P5@mail");
        personService.addPerson("P6F", "P6L", "P6+23", "P6@mail");
    }
    //USE-CASE "SEE USER ORDERS"
    private List<Order> seeUserOrders(Integer pageNumber, Integer pageSize, String personEmail){

        return orderService.getOrdersPageByPersonEmail(pageNumber, pageSize, personEmail);
    }
    //USE-CASE "SEE VEHICLE LOCATION"
    private  Double[]  seeVehicleLocation (Integer vehicleId) {
            Double locations[] = new Double[2];
            Vehicle vehicle = vehicleService.getVehicleByIdentity(vehicleId);
            locations[0] = vehicle.getLocationPointX();
            locations[1] = vehicle.getLocationPointY();
            return locations;
    }

    //USE-CASE "SEE VEHICLE ORDERS"
    private List<Order> seeVehicleOrders(Integer vehicleId, Integer pageNumber, Integer pageSize) {
        Vehicle vehicle = vehicleService.getVehicleByIdentity(vehicleId);
        return orderService.getVehicleOrdersPage(vehicle, pageNumber, pageSize);
    }

    //USE-CASE "ADD VEHICLE"
    private  void addVehicle(String vehicleModel,
                             Date vehicleRegistrationDate,
                             Date vehicleProductionDate,
                             Double vehicleMaxCargo,
                             Double vehicleMaxVolume,
                             Double vehicleCurrentPointX,
                             Double vehicleCurrentPointY) {
        vehicleService.addVehicle(vehicleProductionDate,
                                    vehicleMaxCargo,
                                    vehicleMaxVolume,
                                    0.0,
                                    vehicleModel,
                                    vehicleRegistrationDate,
                                    vehicleCurrentPointX,
                                    vehicleCurrentPointY);
    }
    //USE-CASE "SEE ORDERS"
    private List<Order> seeOrders(Integer pageNumber, Integer pageSize){

        return orderService.gettOrdersRange(pageNumber,pageSize);

    }
}

package delivery.test.database.usecases;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.*;
import delivery.data.services.DateConverter;
import delivery.data.services.GoodsService;
import delivery.data.services.OrderService;
import delivery.data.services.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.OrderBy;
import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class PersonActivitiesTest {

    @Autowired
    PersonService personService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    OrderService orderService;

    @Test
    @Transactional
    public void personActivitiesTest() {
        //registration test
        registration("PersonOne", "PersonOne", "+2345678", "personOne@com");
        //second time to check "if-user-exist"
        registration("PersonOne", "PersonOne", "+2345678", "personOne@com");
        checkPersonsAvailability();
        //get all goods test
        fillGoodsTable();
        for(int i = 0; i < 6 ;i++) {
            try {
                if(seeAllGoods(i,2).size() > 0) {
                    System.out.println("--page-" + i + "--");
                    for (Goods goo : seeAllGoods(i, 2)) {

                        System.out.println(goo.getId() + ":" +
                                goo.getName() + ":" +
                                goo.getCargo() + ":" +
                                goo.getVolume() +
                                goo.getExpirationDate());
                    }
                } else {

                    break;

                }
                Thread.sleep(300);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //create order test
        System.out.println("Create order test...");
        Set<Integer> choosenGoods = new HashSet<Integer>();
        choosenGoods.add(1);
        choosenGoods.add(4);
        choosenGoods.add(6);
        createOrder("personOne@com", choosenGoods);
        Person personHavingOrders = personService.getPersonByEmail("personOne@com");
        Set<Order> personHavingOrdersOrders = new HashSet<Order>();
        personHavingOrdersOrders = personHavingOrders.getOrders();
        for (Order ord: personHavingOrdersOrders) {
            Set<Goods> ordGoods = new HashSet<Goods>();
            ordGoods = ord.getGoods();
            for(Goods goo: ordGoods) {
                System.out.println("Goods: " + goo.getId() + "|" + goo.getName());
            }
        }
        //create order test - add second order
        System.out.println("Create order test (second test)...");
        choosenGoods.clear();
        choosenGoods.add(2);
        choosenGoods.add(3);
        choosenGoods.add(5);
        createOrder("personOne@com", choosenGoods);
        Set<Order> personHavingOrdersOrdersTwo = personHavingOrders.getOrders();
        for (Order ord: personHavingOrdersOrdersTwo) {
            Set<Goods> ordGoods = new HashSet<Goods>();
            ordGoods = ord.getGoods();
            for(Goods goo: ordGoods) {
                System.out.println("Goods: " + goo.getId() + "|" + goo.getName());
            }
        }
        //cancel last order test
        System.out.println("Cancel last order test...");
        personService.getPersonByEmail("personOne@com").getOrders().last().setState(OrderState.PERFORMING);
        cancelOrder("personOne@com");
        System.out.println("Cancel last order test(second attempt)...");
        personService.getPersonByEmail("personOne@com").getOrders().last().setState(OrderState.WAIT);
        cancelOrder("personOne@com");
        Set<Order> personHavingOrdersOrdersThree = personHavingOrders.getOrders();
        for (Order ord: personHavingOrdersOrdersThree) {
            Set<Goods> ordGoods = new HashSet<Goods>();
            ordGoods = ord.getGoods();
            for(Goods goo: ordGoods) {
                System.out.println("Goods: " + goo.getId() + "|" + goo.getName());
            }
        }


    }

    //USE-CASE "REGISTRATION(CHECK IN TRANSACTIONAL AND MULTITHREADING)"
    private boolean registration(String personName, String personSurname, String personPhoneNumber, String personEmail){
        if(personService.getPersonByEmail(personEmail) == null) {
            if (personService.addPerson(personName, personSurname, personPhoneNumber, personEmail)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    private void checkPersonsAvailability(){
        List<Person> persons = new ArrayList();
        persons = (List<Person>) personService.getAllPersons();
        for(Person pers: persons) {
            System.out.println(pers.getName() + ":" +
                    pers.getSurname() + ":" +
                    pers.getPhoneNumber() + ":" +
                    pers.getEmail());
        }

    }
    //USE-CASE "SEE ALL GOODS"
    private List<Goods> seeAllGoods(Integer pageNumber, Integer pageSize){
        return goodsService.getGoodsPage(pageNumber, pageSize);
    }

    private void fillGoodsTable() {
        DateConverter dateConverter = new DateConverter();
        Date dateOne = dateConverter.convertFromString("22/11/2017 09:00:45");
        Date dateTwo = dateConverter.convertFromString("23/11/2017 10:01:45");
        Date dateThree = dateConverter.convertFromString("23/11/2017 10:01:46");
        Date dateFour = dateConverter.convertFromString("24/11/2017 1:59:46");
        Date dateFive = dateConverter.convertFromString("25/11/2017 16:01:46");
        Date dateSix = dateConverter.convertFromString("26/11/2017 11:45:46");
        goodsService.addGoods("goodsOne", 0.1, 0.1, dateOne);
        goodsService.addGoods("goodsTwo", 0.2, 0.2, dateTwo);
        goodsService.addGoods("goodsThree", 0.3, 0.3, dateThree);
        goodsService.addGoods("goodsFour", 0.4, 0.4, dateFour);
        goodsService.addGoods("goodsFive", 0.5, 0.5, dateFive);
        goodsService.addGoods("goodsSix", 0.6, 0.6, dateSix);
    }
    //USE-CASE "CREATE ORDER"(CHOOSE A GOODS)
    private void createOrder(String personEmail, Set<Integer> goodsIds){

        Person personCurrent = personService.getPersonByEmail(personEmail);
        Order personOrder = new Order();
        Set<Goods> personGoods = new HashSet<Goods>();
        personGoods = goodsService.getGoodsByIdsRange(goodsIds);
        personOrder.setGoods(personGoods);
        if(personCurrent.getOrders() == null) {
            SortedSet<Order> orders = new TreeSet<Order>();
            orders.add(personOrder);
            personCurrent.setOrders(orders);
        } else {
            personCurrent.getOrders().add(personOrder);
        }

    }
    //USE-CASE "CANCEL ORDER"
    private void cancelOrder(String personEmail){
        Person personCancelling = new Person();
        personCancelling = personService.getPersonByEmail(personEmail);
        if(personCancelling.getOrders().last().getState() == OrderState.WAIT) {
            personCancelling.getOrders().remove(personCancelling.getOrders().last());
        } else {
            System.out.println("Can not cancel order.");
        }
    }
}

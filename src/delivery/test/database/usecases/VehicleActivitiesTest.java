package delivery.test.database.usecases;

import delivery.config.PersistenceJPAConfig;
import delivery.data.model.*;
import delivery.data.services.DateConverter;
import delivery.data.services.OrderService;
import delivery.data.services.VehicleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class VehicleActivitiesTest {

    @Autowired
    VehicleService vehicleService;
    @Autowired
    OrderService orderService;

    @Test
    @Transactional
    public void vehicleActivitiesTest() {

        createVehicles();
        checkVehicleAvailability();
        updateSelfState(1);
        updateVehicleLocation(1);
        checkVehicleAvailability();
        createOrders();
        checkOrdersAvailability();
        //setting vehicle order (for test only)
        Vehicle vehicleOneChoosen = vehicleService.getVehicleByIdentity(3);
        System.out.println("Choosen vehicle...");
        System.out.println(vehicleOneChoosen.getId() + "|" +
                            vehicleOneChoosen.getVehicleState() + "|" +
                            vehicleOneChoosen.getVehicleModel());
        //setting first order
        Order orderVehicleOneChoosen = orderService.getActuallyOrder();
        orderVehicleOneChoosen.setState(OrderState.PERFORMING);
        Set<Order> vehiclOneChoosenOrders = new HashSet<Order>();
        vehiclOneChoosenOrders.add(orderVehicleOneChoosen);
        vehicleOneChoosen.setOrders(vehiclOneChoosenOrders);
        //complete vehicle order(MUST TEST IN MULTIHTHREADING + TRANSACTIOANAL)
        completeOrder(vehicleOneChoosen.getId());
        //test result after order complition
        System.out.println("Check order complition...");
        for(Order ord: vehicleOneChoosen.getOrders()) {
            System.out.println(ord.getId() + "|" + ord.getState());
        }
        System.out.println("--orders--");
        for(Order ord: orderService.getAllOrders()) {
            System.out.println(ord.getId() + "|" + ord.getState());
        }
        //set vehicle state
        System.out.println("Checck vahicle state change...");
        System.out.println("Vehicle state before state change...");
        System.out.println(vehicleOneChoosen.getVehicleState());
        vehicleOneChoosen.setVichecleState(VehicleState.BUZY);
        System.out.println("Vehicle state after state change...");
        System.out.println(vehicleOneChoosen.getVehicleState());
        //update vehicle location
        for(int i = 0; i <10 ;i++) {
            try {
                System.out.println("New vehicle location");
                updateVehicleLocation(vehicleOneChoosen.getId());
                Thread.sleep(1000);
                Vehicle checkedVehicle = vehicleService.getVehicleByIdentity(vehicleOneChoosen.getId());
                System.out.println(checkedVehicle.getLocationPointX() + "|" + checkedVehicle.getLocationPointY());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createVehicles() {
        System.out.println("Create vehicles...");
        DateConverter dateConverter = new DateConverter();
        Date dateTest = dateConverter.getCurrentDate();
        vehicleService.addVehicle(dateTest, 23.44, 0.23, 0.45, "Vehicle One", dateTest, 144.1, 345.67);
        vehicleService.addVehicle(dateTest, 24.44, 0.24, 0.46, "Vehicle Two", dateTest, 120.1, 200.67);
        vehicleService.addVehicle(dateTest, 25.44, 0.25, 0.47, "Vehicle Three", dateTest, 142.1, 145.67);
        vehicleService.addVehicle(dateTest, 26.44, 0.26, 0.48, "Vehicle Four", dateTest, 120.1, 200.67);
        vehicleService.addVehicle(dateTest, 27.44, 0.27, 0.49, "Vehicle Five", dateTest, 500.1, 700.67);
    }

    private void checkVehicleAvailability() {
        List<Vehicle> vehiclesResult = vehicleService.getAllVehicles();
        for (Vehicle veh: vehiclesResult) {
            System.out.println(veh.getId() + "|" + veh.getVehicleState() + "|" + veh.getLocationPointX() + "|" + veh.getLocationPointY());
        }
    }

    //USE-CASE "UPDATE LOCATION"
    private boolean updateVehicleLocation(Integer vehicleId){

        if(vehicleService.setVehicleLocation(generateRandomDouble(), generateRandomDouble(),vehicleId)) {

            return true;

        } else  {

            return false;

        }

    }

    //USE-CASE "UPDATE SELF STATE"
    private boolean updateSelfState(Integer vehicleId) {
        if(vehicleService.setVehicleState(VehicleState.BUZY,vehicleId)) {

            return true;

        } else  {

            return false;

        }
    }

    private double generateRandomDouble() {

        final double rangeMin = 0;
        final double rangeMax = 100;
        Random random = new Random();

        return rangeMin + (rangeMax - rangeMin) * random.nextDouble();
    }

    //+USE-CASE "COMPLETE ORDER"
    private  boolean completeOrder(Integer vehicleId) { // identity by id because it's a best to storage in vehicle own memory
                                                        // vehicle send his id to a server...
        Vehicle vehicle = vehicleService.getVehicleByIdentity(vehicleId);
        List<Order> completedOrders = new ArrayList<Order>();
        completedOrders = orderService.getVehiclePreformingOrders(vehicle, OrderState.PERFORMING);
        for(Order ord: completedOrders) {
            ord.setState(OrderState.COMPLETED);
        }

        Set<Order> vehicleOrders = new HashSet<Order>();
        Order nextOrder = orderService.getActuallyOrder();
        nextOrder.setState(OrderState.PERFORMING);
        vehicle.getOrders().add(nextOrder);

        return true;

    }

    private void createOrders(){
        System.out.println("Create orders...");
        DateConverter dateConverter = new DateConverter();
        Date dateTest = dateConverter.getCurrentDate();
        Date dateOne = dateConverter.convertFromString("22/11/2017 09:00:45");
        Date dateTwo = dateConverter.convertFromString("23/11/2017 10:01:45");
        Date dateThree = dateConverter.convertFromString("23/11/2017 10:01:46");
        orderService.addOrder(0.23, 0.24, dateOne, OrderState.WAIT);//next waiting order
        orderService.addOrder(1.12, 234.98, dateTwo, OrderState.WAIT);//next waiting order
        orderService.addOrder(44.25, 45.98, dateThree, OrderState.WAIT);//next waiting order
        orderService.addOrder(0.25, 0.44, dateTest, OrderState.COMPLETED);
        orderService.addOrder(0.55, 0.74, dateTest, OrderState.COMPLETED); // current order
        orderService.addOrder(0.82, 0.98, dateTest, OrderState.COMPLETED);
    }

    private void checkOrdersAvailability() {
        List<Order> resultOrders = orderService.getAllOrders();
        for (Order ord: resultOrders) {
            System.out.println(ord.getId() + "|" +
                            ord.getLocationPointX() + "|" +
                            ord.getLocationPointY() + "|" +
                            ord.getDate() + "|" +
                            ord.getState());
        }
    }
}

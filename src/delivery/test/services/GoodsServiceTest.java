package delivery.test.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.GoodsRepository;
import delivery.data.model.Goods;
import delivery.data.model.Warehouse;


@Service
@Transactional
public class GoodsServiceTest {
	
	private final GoodsRepository goodsRepository;
	
	@Autowired(required=true)
	 public GoodsServiceTest(GoodsRepository goodsRepository) {
			    this.goodsRepository = goodsRepository;			    
	}
	
	public void createGoodsTest() {
		System.out.println("Goods creation...");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		Goods g1 = new Goods("goodsOne", 1.23, 0.02, date1);
		goodsRepository.save(g1);
	}
	
	public void selectGoodsTest() {
		System.out.println("Get goods:");
		for(Goods g: goodsRepository.findAll()) {
			System.out.println(g.getName() + ":"
							 + g.getCargo()  + ":"
							 + g.getVolume()  + ":"
							 + g.getExpirationDate() + ":"
							 );
		}
	}
}

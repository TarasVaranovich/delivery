package delivery.test.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.OrderRepository;
import delivery.data.model.Goods;
import delivery.data.model.Order;
import delivery.data.model.OrderState;

@Service
@Transactional
public class OrderServiceTest {
	
	private final OrderRepository orderRepository;
	
	@Autowired(required=true)
	public OrderServiceTest(OrderRepository orderRepository){
		this.orderRepository = orderRepository;
	}
	
	public void createOrderTest() {
		
		System.out.println("Order creation...");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		Order o1 = new Order(210.1, 220.2, date1, OrderState.WAIT);
		orderRepository.save(o1);
	}
	
	public void selectOrderTest() {
		System.out.println("Get orders:");
		for(Order o: orderRepository.findAll()) {
			System.out.println(o.getLocationPointX() + ":"
							 + o.getLocationPointY()  + ":"
							 + o.getDate().toString() + ":"
							 + o.getState()+ ":"
							 );
		}
	}
	
	public void setGoodsTest(Set<Goods> goods) {
		System.out.println("Setting order goods...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		Order or1 = new Order(210.1, 220.2, date1, OrderState.WAIT);
		or1.setGoods(goods);
		orderRepository.save(or1);
	}
	
	
	public void getGoodsTest() {
		System.out.println("Get order goods:");
		for(Order or: orderRepository.findAll()) {
			Set<Goods> orderGoods = or.getGoods();
			System.out.println("Goods count:" + orderGoods.size());
		}
	}
	
	/*public void setVehiclesTest(Set<Vehicle> vehicles) {
		System.out.println("Setting order vehicles...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		Order or3 = new Order(255.1, 255.2, date1, OrderState.WAIT);
		or3.setVehicles(vehicles);
		orderRepository.save(or3);
	}
	
	public void getVehiclesTest() {
		System.out.println("Get order vehicles:");
		for(Order ord: orderRepository.findAll()) {
			Set<Vehicle> orderVehicles = ord.getVehicles();
			System.out.println("Vehicles count:" + orderVehicles.size());
		}
	}*/
}

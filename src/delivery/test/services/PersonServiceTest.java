package delivery.test.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.PersonRepository;
import delivery.data.model.Order;
import delivery.data.model.Person;

@Service
@Transactional
public class PersonServiceTest {
	
	private final PersonRepository personRepository;
	
	@Autowired(required=true)
	public PersonServiceTest(PersonRepository personRepository) {
		
		this.personRepository = personRepository;
		
	}
	
	public void createPersonTest() {
		
		System.out.println("Person creation...");
		Person p1 = new Person("PersonOne", "PersonOneSur", "+000000", "personOne@gmail.com");
		personRepository.save(p1);
	}
	
	public void selectPersonTest() {
		System.out.println("Get persons:");
		for(Person p: personRepository.findAll()) {
			System.out.println(p.getName() + ":"
							 + p.getSurname()  + ":"
							 + p.getPhoneNumber()  + ":"
							 + p.getEmail() + ":"
							 );
		}
	}
	
	public void setOrdersTest(Set<Order> orders) {
		System.out.println("Setting person orders...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		Person p2 = new Person("PersonTwo", "PersonTwoSur", "+000000", "personTwo@gmail.com");
		p2.setOrders((SortedSet) orders);;
		personRepository.save(p2);
	}
	
	
	public void getOrdersTest() {
		System.out.println("Get person orders:");
		for(Person per: personRepository.findAll()) {
			Set<Order> personOrders = per.getOrders();
			System.out.println("Orders count:" + personOrders.size());
		}
	}
}

package delivery.test.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.VehicleRepository;
import delivery.data.model.Vehicle;
import delivery.data.model.Order;

import java.util.Date;
import java.util.Set;
import java.text.SimpleDateFormat;

@Service
@Transactional
public class VechicleServiceTest {
		
	private final VehicleRepository vechicleRepository; 
	
	@Autowired(required=true)
	 public VechicleServiceTest(VehicleRepository vechicleRepository) {
			    this.vechicleRepository = vechicleRepository;			    
	}

	
	public void createVehicleTest() {
		System.out.println("Vehicle creation...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		Vehicle v1 = new Vehicle("Name1", date1, date1, 55.5, 0.75, 100.0, 100.0);
		vechicleRepository.save(v1);
	}
	
	public void selectVehicleTest() {
		System.out.println("Get vehicles:");
		for(Vehicle v: vechicleRepository.findAll()) {
			System.out.println(v.getVehicleModel() + ":"
							 + v.getCreationDate()  + ":"
							 + v.getVehicleRegistrationDate()  + ":"
							 + v.getMaxCargo()  + ":"
							 + v.getMaxVolume() + ":"
							 + v.getLocationPointX() + ":"
							 + v.getLocationPointY() + ":"
							 + v.getVehicleState().toString());
		}
	}
	
	public void setOrdersTest(Set<Order> orders) {
		System.out.println("Setting vehicle orders...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		Vehicle v2 = new Vehicle("NameTwo", date1, date1, 55.5, 0.75, 100.0, 100.0);
		v2.setOrders(orders);
		vechicleRepository.save(v2);
	}
	
	public void getOrdersTest() {
		System.out.println("Get vehicles orders:");
		for(Vehicle veh: vechicleRepository.findAll()) {
			Set<Order> vehicleOrders = veh.getOrders();
			System.out.println("Orders count:" + vehicleOrders.size());
		}
	}
	
}

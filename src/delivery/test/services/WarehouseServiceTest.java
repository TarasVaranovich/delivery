package delivery.test.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import delivery.data.jpa.WarehouseRepository;

import delivery.data.model.Warehouse;
import delivery.data.model.Goods;


@Service
@Transactional
public class WarehouseServiceTest {
	
	private final WarehouseRepository warehouseRepository; 
	
	@Autowired(required=true)
	 public WarehouseServiceTest(WarehouseRepository warehouseRepository) {
			    this.warehouseRepository = warehouseRepository;			    
	}
	
	public void createWarehouseTest() {
		System.out.println("Warehouse added...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		Warehouse w1 = new Warehouse(1.5, 2.3, date1, 0.76, 0.9);
		warehouseRepository.save(w1);
	}
	
	public void selectWarehouseTest() {
		System.out.println("Get warehouse data:");
		for(Warehouse wh: warehouseRepository.findAll()) {
			System.out.println(wh.getCreationDate() + ":" 
							 + wh.getLocationPointX() + ":" 
							 + wh.getLocationPointY() 
							 + wh.getMaxCargo() + ":" 
							 + wh.getMaxVolume() + ":" 
							 + wh.getTotalCargo() + ":" 
							 + wh.getMaxVolume());
		}
	}
	
	public void setGoodsTest(Set<Goods> goods) {
		System.out.println("Setting warehouse goods...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = null;
		
		try {
			
			date1 = sdf.parse("21/12/2012");
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		Warehouse w1 = new Warehouse(1.5, 2.3, date1, 0.76, 0.9);
		w1.setGoods(goods);
		warehouseRepository.save(w1);
	}
	
	
	public void getGoodsTest() {
		System.out.println("Get warehouse goods:");
		for(Warehouse wh: warehouseRepository.findAll()) {
			Set<Goods> warehouseGoods = wh.getGoods();
			System.out.println("Goods count:" + warehouseGoods.size());
		}
	}
}

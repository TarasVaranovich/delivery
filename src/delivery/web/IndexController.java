package delivery.web;

import java.util.Set;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import delivery.data.jpa.VehicleRepository;
import delivery.data.jpa.WarehouseRepository;
import delivery.data.jpa.GoodsRepository;
import delivery.data.jpa.PersonRepository;
import delivery.data.jpa.OrderRepository;

import delivery.data.model.Goods;
import delivery.data.model.Order;
import delivery.data.model.OrderState;
import delivery.data.model.Vehicle;
import delivery.test.services.GoodsServiceTest;
import delivery.test.services.OrderServiceTest;
import delivery.test.services.PersonServiceTest;
import delivery.test.services.VechicleServiceTest;
import delivery.test.services.WarehouseServiceTest;

@Controller
@RequestMapping("/index.htm")
public class IndexController {
	
	@Autowired(required=true)
	private VehicleRepository vehicleRepository;
	@Autowired(required=true)
	private WarehouseRepository warehouseRepository; 
	@Autowired(required=true)
	private GoodsRepository goodsRepository; 
	@Autowired(required=true)
	private PersonRepository personRepository;
	@Autowired(required=true)
	private OrderRepository orderRepository;
	
	@RequestMapping(method = GET)
	 public String form(Model model) {
		    return "delivery.html";
	}
	
	@Transactional /*read-only, propagation and other settings*/
	@RequestMapping(value="create.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody			
	public void createTables() {
		VechicleServiceTest vechicleService = new VechicleServiceTest(vehicleRepository);
		vechicleService.createVehicleTest();
		WarehouseServiceTest warehouseService =  new WarehouseServiceTest(warehouseRepository);
		warehouseService.createWarehouseTest();
		GoodsServiceTest goodsService = new GoodsServiceTest(goodsRepository);
		goodsService.createGoodsTest();
		PersonServiceTest personService = new PersonServiceTest(personRepository);
		personService.createPersonTest();
		OrderServiceTest orderService = new OrderServiceTest(orderRepository);
		orderService.createOrderTest();
		WarehouseServiceTest warehouseService2 =  new WarehouseServiceTest(warehouseRepository);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		OrderServiceTest orderService2 = new OrderServiceTest(orderRepository);
		PersonServiceTest personService2 = new PersonServiceTest(personRepository);
		VechicleServiceTest vechicleService2 = new VechicleServiceTest(vehicleRepository);
		
		try {
			
			Date dateTest = sdf.parse("21/12/2012");
			
			Set goodsTest = new HashSet<Goods>(){{
	            add(new Goods("goodsOne", 1.23, 0.02, dateTest));
	            add(new Goods("goodsTwo", 1.23, 0.02, dateTest));
	            add(new Goods("goodsThree", 1.23, 0.02, dateTest));
	        }};
			warehouseService2.setGoodsTest(goodsTest);
			
			Set goodsTestOrder = new HashSet<Goods>(){{
	            add(new Goods("goodsFour", 1.23, 0.02, dateTest));
	            add(new Goods("goodsFive", 1.23, 0.02, dateTest));
	            add(new Goods("goodsSix", 1.23, 0.02, dateTest));
	        }};
			orderService2.setGoodsTest(goodsTestOrder);
			
			Set ordersTestPerson = new HashSet<Order>(){{
	            add(new Order(211.1, 221.2, dateTest, OrderState.WAIT));
	            add(new Order(212.1, 222.2, dateTest, OrderState.WAIT));
	            add(new Order(213.1, 223.2, dateTest, OrderState.WAIT));
	        }};
	        personService2.setOrdersTest(ordersTestPerson);
	        Set ordersTestVehicle = new HashSet<Order>(){{
	            add(new Order(214.1, 224.2, dateTest, OrderState.WAIT));
	            add(new Order(215.1, 225.2, dateTest, OrderState.WAIT));
	            add(new Order(216.1, 226.2, dateTest, OrderState.WAIT));
	        }};
	        vechicleService2.setOrdersTest(ordersTestVehicle);
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
	}
	
	@Transactional/*for fetching one-to-many entities*/
	@RequestMapping(value="getAll.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody			
	public void getAllData() {
		VechicleServiceTest vechicleService = new VechicleServiceTest(vehicleRepository);
		vechicleService.selectVehicleTest();
		WarehouseServiceTest warehouseService =  new WarehouseServiceTest(warehouseRepository);
		warehouseService.selectWarehouseTest();
		GoodsServiceTest goodsService = new GoodsServiceTest(goodsRepository);
		goodsService.selectGoodsTest();
		PersonServiceTest personService = new PersonServiceTest(personRepository);
		personService.selectPersonTest();
		OrderServiceTest orderService = new OrderServiceTest(orderRepository);
		orderService.selectOrderTest();
		WarehouseServiceTest warehouseService3 =  new WarehouseServiceTest(warehouseRepository);
		warehouseService3.getGoodsTest();
		OrderServiceTest orderService3 = new OrderServiceTest(orderRepository);
		orderService3.getGoodsTest();
		PersonServiceTest personService3 = new PersonServiceTest(personRepository);
		personService3.getOrdersTest();
		VechicleServiceTest vehicleService3 = new VechicleServiceTest(vehicleRepository);
		vehicleService3.getOrdersTest();
	}
	
}
